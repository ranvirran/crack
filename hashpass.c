#include <stdio.h>
#include <stdlib.h>
#include "md5.h"

int main(int argc, char *argv[])
{

    FILE * fp_input;
    FILE * fp_output;
    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    
    fp_input = fopen(argv[1],"r");
    fp_output = fopen(argv[2],"w");
    
    
    while ((read = getline(&line, &len, fp_input)) != -1) {
        
        line = strtok (line, "\n");
        fprintf (fp_output, "%s\n", md5(line, (int) read - 1));
    }


    fclose(fp_input);
}